<?php

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Form builder.
 */
function fsw_pickup_settings_form($form, &$form_state) {
  $form['fsw_pickup_variable_foo'] = [
    '#type' => 'textfield',
    '#title' => t('Foo'),
    '#default_value' => variable_get('fsw_pickup_variable_foo', 42),
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}
