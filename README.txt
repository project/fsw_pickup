Ubercart Branch Pickup
======

Adds an option for an Ubercart order to be collected from a selected list of branches.

Individual products may be limited to one or several branches - or available 
to be picked up at any branch (the default behaviour).

Branches are read from a branch content type - see installation instructions below.
This may get wrapped up into a feature in the future.
The branch content type can be used to show branch details to visitors as part
of the site - only the title and address field are required, any others may be added as required.


Requirements
============

Address Field module 7.x-1.2
Ubercart 3.x (Drupal 7)


Installation
============

1. Create a Branch content type with Machine name: branch
2. Leave the node title and add at least an address field named Address with machine name: field_address
3. Enable the module
4. Add branches
5. (Optional) configure any products to be limited to one or more branches
